#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
int main()
{
  //it Need to algorithm to accurate a Numbers 
  int *mainArr; //dynamic
  int maxCount = 0, count = 0;
  int Mode, countingLoopMode = 0;
  int *TestinMode2; //dynamic
  int usrIn, numberInputed;
  printf("Enter a Length N: ");
  scanf("%d", &usrIn);

  mainArr = (int*)malloc(sizeof(int) * usrIn);
  TestinMode2 = (int*)malloc(sizeof(int) * usrIn);
  
  if(mainArr == NULL && TestinMode2 == NULL)
  {
    printf("Error!");
    return 1;
  }
  for(int i = 0; i < usrIn; i++)
  {
    printf("Enter a Number: ");
    scanf("%d", &numberInputed);
    mainArr[i] = numberInputed;
  }

  //bubble Sort
  for(int i=0;i<usrIn-1;i++)
  {
    for(int j=0;j<usrIn-1;j++)
    {
      if(mainArr[j] > mainArr[j + 1])
      {
        int temp = mainArr[j];
        mainArr[j] = mainArr[j+1];
        mainArr[j+1] = temp;
      }
    }
  }
  //Printing Sorted Arrays
  for(int i = 0; i < usrIn; i++)
  {
    printf("Sorted Input: %d\n", mainArr[i]);
  }

   //Main Machine Mode
  for(int i = 0; i < 10; i++)
  {
    count = 0;
    for(int j = 0; j < 10; j++)
    {
      if(mainArr[i] == mainArr[j]) //Double Numbers only
      {
      count++;
      }
    }
    if(count > maxCount) //store highest Mode
    {
      maxCount = count;
      TestinMode2[0] = mainArr[i];
      countingLoopMode = 1;
    } else if(maxCount == count) //Store most Frequent Numbers
    {
      TestinMode2[countingLoopMode] = mainArr[i];
      countingLoopMode++;
    }
  }
  for(int i = 0; i < countingLoopMode; i++)//Will print once 
  {
    count++;
    if(count >= maxCount)
    {
      maxCount = count;
      printf("%d\n", TestinMode2[i]);
      count = 0;
    }
  }
  free(mainArr);
  free(TestinMode2);
  return 0;
}
